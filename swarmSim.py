'''
*******************************************************************
Swarm simulation for maya
Copyright (C) 2016  Davide Passaniti
email: 1300278@uad.ac.uk

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*******************************************************************
How to use:

- The maya scene can be empty or not, so long ad you add this expression
  in the expression editor:

  python("run("+frame+")")

  Then copy paste and run the script in the maya script editor.
  Keep in mind that objects with a name that starts with 'boid' 
  might be deleted during use.

- The 'reset and respawn' button in the scene control tab of the GUI
  can be used to spawn a number of boids in a radius around the
  origin. It will also stop any ongoing simulation, reset the
  timeline to frame 1 and delete all existing boids.
  
- The algorithms tab contains various options to control the
  simulation. Generally speaking, setting the strenght of an 
  algorithm to 0 will disable that algorithm. You can change these 
  settings before starting the simulation or while it's running 
  without problems
  
- The GUI should be self descriptive and straightforward to use. But
  here are a few 'tips':
  - Any object can be used as chase/flee target, while for the 
    containment you must use a polyCube and for the obstacle avoidance 
    you must use a polySphere (as written on the buttons)
  - You can animate chase/flee targets with keyframes, leave them 
    still or pause the simulation or move them manually and then restart
  - The 'Dynamic predator checkbox' for the flee algorithm will make 
    the loaded target chase the flock like a predator
  - You can have only 1 flee target, 1 chase target and 1 containment 
    area, but you can add as many obstacles as you wish
  - To remove a target from the simulation, load a new one or delete 
    it from the scene. To remove an obstacle delete it from the scene.
  - If you load a containment area you can delete it and it will still
    be in effect. Alternatively you can turn on wireframe mode in maya 
    to see through the object

- If you run part of the simulation, then bring back the timeline to 
  1 and change some values and start again, what you will see is the 
  simulation maya already calculated. So remember to use 'reset and 
  respawn boids' and delete the predators if dynamic was checked. 
  (not necessary for objects that don't move like static targets, 
  containment area and obstacles)
  
- If you pause the simulation and manually change the frame on the 
  timeline, when you restart it there might be some bugs(especially 
  after reaching the point where you stopped the first run). You 
  can do this to rewatch a part of the simulation from a different 
  perspective, but remember to 'reset and respawn boids' after
  
************************************************************************
'''

import maya.cmds, maya.mel
import copy, math, random
import maya.api.OpenMaya as om

# *** GLOBALS - FLAGS - CLASSES ***

boids = []
obstacles = []
externalCurrentHdn = om.MVector(0,0,0)
lastFrame = 1
containmentArea = [om.MVector(0,0,0), om.MVector(0,0,0)]
neighbourRange = 5
fleeRange = 35
avoidRange = 10
chaseTargetName = ''
predatorName = ''

applyChasing = False
applyFleeing = False
dynamicPredator = False
applyContainment = False
applyAvoidance = False

# heading modifiers
cohesionStrenght = 0.2
separationStrenght = 1
alignmentStrenght = 0.3
chaseStrenght = 0
fleeStrenght = 0
containmentStrenght = 0
obstacleAvoidanceStrenght = 0

# return a vector with the position of an object from the scene
def getPositionOf(name):    
    x = cmds.getAttr(name+'.translateX')
    y = cmds.getAttr(name+'.translateY')
    z = cmds.getAttr(name+'.translateZ')
    return om.MVector(x,y,z)

# boid class, holds all physics and scene info of a boid
class boid(object):
    def __init__(self, name, startPos, startRot):
        self.name = name
        self.id = self.name[-1:]
        self.startPos = startPos
        self.startRot = startRot
        self.pos = om.MVector(self.startPos.x, self.startPos.y, self.startPos.z)
        self.rot = om.MVector(self.startRot.x, self.startRot.y, self.startRot.z)
        self.speed = 25
        self.maxRot = 5#maximum rotation change per frame (degrees)
    #set the attributes of the object in maya
    def applyData(self):
        cmds.setAttr(self.name+'.rotateX', self.rot.x)
        cmds.setAttr(self.name+'.rotateY', self.rot.y)
        cmds.setAttr(self.name+'.rotateZ', self.rot.z)
        cmds.setAttr(self.name+'.translateX', self.pos.x)
        cmds.setAttr(self.name+'.translateY', self.pos.y)
        cmds.setAttr(self.name+'.translateZ', self.pos.z)
    #set current values to starting values
    def reset(self):
        pass
        self.pos = om.MVector(self.startPos.x, self.startPos.y, self.startPos.z)
        self.rot = om.MVector(self.startRot.x, self.startRot.y, self.startRot.z)
    def headingVec(self):
        zAxis = om.MVector(0.0,0.0,1.0)#reference axis is z
        rad = [math.radians(self.rot.x), math.radians(self.rot.y), math.radians(self.rot.z)]
        rotation = om.MEulerRotation(rad[0], rad[1], rad[2])
        heading = zAxis.rotateBy(rotation)
        heading.normalize()
        return heading
     
# *** SIMULATION ALGORITHMS ***

# calculate heading towards perceived centre of mass of the swarm
def cohesion(currentBoid):
    global boids
    swarmCentre = om.MVector(0.0,0.0,0.0)
    # find perceived centre of swarm
    for boid in boids:
        if(boid.id != currentBoid.id):
            swarmCentre += boid.pos
    swarmCentre /= (len(boids) - 1)
    vecToCentre = swarmCentre - currentBoid.pos
    vecToCentre.normalize()
    return vecToCentre
    
# calculate heading to avoid collision with other boids
def separation(currentBoid):
    global boids, neighbourRange
    separationVec = om.MVector(0.0,0.0,0.0)
    for boid in boids:
        if(boid.id != currentBoid.id):
            dist = boid.pos - currentBoid.pos
            if(dist.length() < neighbourRange):
                separationVec -= dist
    separationVec.normalize()
    return separationVec

# calculate heading to match velocity of nearby boids
def alignment(currentBoid):
    global boids
    swarmVel = om.MVector(0.0,0.0,0.0)
    # find average swarm heading
    for boid in boids:
        if(boid.id != currentBoid.id):
            swarmVel += boid.headingVec()
    swarmVel /= (len(boids) - 1)
    alignVec = swarmVel - currentBoid.headingVec()
    alignVec.normalize()
    return alignVec

# calculate heading towards the target
def chase(currentBoid, targetPos):
    chaseVec = targetPos - currentBoid.pos
    chaseVec.normalize()
    return chaseVec
    
# calculate heading to flee from target
def flee(currentBoid, targetPos):
    global fleeRange
    fleeVec = om.MVector(0,0,0)
    # if in range, go opposite way, otherwise ignore
    dist = targetPos - currentBoid.pos
    if(abs(dist.length()) < fleeRange):
        fleeVec = currentBoid.pos - targetPos
    fleeVec.normalize()
    return fleeVec

# calculate the heading to remain in the specified area
def containment(boid):
    global containmentArea
    containmentHeading = om.MVector(0,0,0)
    # check x,y,z position is inside area
    if(boid.pos.x < containmentArea[0].x):
        containmentHeading.x = 1
    elif(boid.pos.x > containmentArea[1].x):
        containmentHeading.x = -1
    if(boid.pos.y < containmentArea[0].y):
        containmentHeading.y = 1
    elif(boid.pos.y > containmentArea[1].y):
        containmentHeading.y = -1
    if(boid.pos.z < containmentArea[0].z):
        containmentHeading.z = 1
    elif(boid.pos.z > containmentArea[1].z):
        containmentHeading.z = -1
    # normalize and return
    containmentHeading.normalize()
    return containmentHeading

def obstacleAvoidance(boid):
    global obstacles, avoidRange
    avoidVec = om.MVector(0,0,0)
    #get the closest obstacle
    closest = (getPositionOf(obstacles[0]) - boid.pos).length()
    name = obstacles[0]
    for obs in obstacles:#could skip first but w/e...
        dist = (getPositionOf(obs) - boid.pos).length()
        if(dist < closest):
            closest = dist
            name = obs
    #get obs data
    obsPos = getPositionOf(name)
    obsRadius = cmds.polySphere(name, q=True, r=True) + 2 #slightly larger to account for size of boid
    # get ahead vectors
    aheadFull = boid.pos + boid.headingVec() * avoidRange
    aheadHalf = boid.pos + boid.headingVec() * avoidRange * 0.5        
    # if either intersects, the obstacle is close enough to be relevant and we're going towards it
    if((obsPos - aheadFull).length() < obsRadius or (obsPos - aheadHalf).length() < obsRadius):
        avoidVec = boid.pos - obsPos
    #normalize and return
    avoidVec.normalize()
    return avoidVec
    
# *** SCENE UPDATE FUNCTIONS ***

# move a boid based on the current and new heading
def moveBoid(boid, finalHeading, frameTime):
    # apply new heading to boid, restrict max rotation
    boidHeading = boid.headingVec()
    # get axis angle rotation from current to final heading
    aaRot = cmds.angleBetween(v1=(boidHeading.x, boidHeading.y, boidHeading.z), v2=(finalHeading.x, finalHeading.y, finalHeading.z))
    # if angle is too big, clamp it
    if(abs(aaRot[3]) > boid.maxRot):
        if(aaRot[3] < 0):
            aaRot[3] = -boid.maxRot
        else:
            aaRot[3] = boid.maxRot
    # compose rotation quaternion from axis and new angle
    qw = math.cos(math.radians(aaRot[3]/2))
    qx = aaRot[0] * math.sin(math.radians(aaRot[3]/2))
    qy = aaRot[1] * math.sin(math.radians(aaRot[3]/2))
    qz = aaRot[2] * math.sin(math.radians(aaRot[3]/2))
    # rotate heading
    boidHeading = boidHeading.rotateBy(om.MQuaternion(qx, qy, qz, qw))
    # set boid rotation to x,y,z angles from z axis to the heading
    zToCurrent = cmds.angleBetween(euler=True, v1=(0,0,1), v2=(boidHeading.x, boidHeading.y, boidHeading.z))
    boid.rot = om.MVector(zToCurrent[0], zToCurrent[1], zToCurrent[2])    
    # move the boid based on the adjusted heading and speed
    boid.pos.x += boidHeading.x*(boid.speed*frameTime)
    boid.pos.y += boidHeading.y*(boid.speed*frameTime)
    boid.pos.z += boidHeading.z*(boid.speed*frameTime)            
    # update scene object
    boid.applyData()
    
def update():
    global boids, chaseTargetName, applyChasing, applyFleeing, dynamicPredator, applyContainment, applyAvoidance, externalCurrentHdn, obstacles
    global cohesionStrenght, separationStrenght, alignmentStrenght, chaseStrenght, fleeStrenght, externalCurrentStrenght, containmentStrenght, obstacleAvoidanceStrenght
    
    # calculate duration of a frame for use in the move function
    frameRate = mel.eval("currentTimeUnitToFPS")
    frameTime = 1.0 / frameRate
    
    # check for chase target and predator
    if(cmds.objExists(chaseTargetName)):
        chaseTargetPos = getPositionOf(chaseTargetName)
    else:
        applyChasing = False
        
    if(cmds.objExists(predatorName)):
        predatorPos = getPositionOf(predatorName)
    else:
        applyFleeing = False
    
    # check if some obstacles were deleted   
    deletedObs = []
    for obs in obstacles:
        if(not cmds.objExists(obs)):
            deletedObs.append(obs)
    for obs in deletedObs:
        obstacles.remove(obs)
        
    if(len(obstacles) == 0):
        applyAvoidance = False   
    
    # move predator towards centre of swarm
    if(dynamicPredator and cmds.objExists(predatorName)):
        predHeading = cohesion(predator)
        moveBoid(predator, predHeading, frameTime)
    
    # move each boid
    for boid in boids:
        # 3 core simulation rules
        cohesionHeading = cohesion(boid) * cohesionStrenght
        separationHeading = separation(boid) * separationStrenght
        alignmentHeading = alignment(boid) * alignmentStrenght
        # goal chasing
        chaseHeading = om.MVector(0,0,0)
        if(applyChasing):
            chaseHeading = chase(boid, chaseTargetPos) * chaseStrenght
        # fleeing
        fleeHeading = om.MVector(0,0,0)
        if(applyFleeing):
            fleeHeading = flee(boid, predatorPos) * fleeStrenght
        # containment area
        containmentHeading = om.MVector(0,0,0)
        if(applyContainment):
            containmentHeading = containment(boid) * containmentStrenght
        # obstacle Avoidance
        obstacleAvoidanceHeading = om.MVector(0,0,0)
        if(applyAvoidance):
            obstacleAvoidanceHeading = obstacleAvoidance(boid) * obstacleAvoidanceStrenght        
        # put all factors together for the final heading
        finalHeading = cohesionHeading + separationHeading + alignmentHeading + chaseHeading + fleeHeading + externalCurrentHdn + containmentHeading + obstacleAvoidanceHeading
        finalHeading.normalize()
        #move
        moveBoid(boid, finalHeading, frameTime)
        
def run(currentFrame):
    global lastFrame, boids
    if(currentFrame == 1):
        for boid in boids:
            boid.reset()
            boid.applyData()
    elif(currentFrame - lastFrame == 1):
        update()
    lastFrame = currentFrame

# *** GUI ***

# commands
def reset(arg):
    global boids
    # stop sim
    cmds.play(st=False)
    cmds.currentTime(1)
    # clear boids and boid list
    boids = []
    if(cmds.objExists('boid0')):
        cmds.select('boid*')
        cmds.delete()
    #spawn in area with random values
    r = int(cmds.textField(radius, query=True, text=True))
    n = int(cmds.textField(number, query=True, text=True))
    s = int(cmds.textField(speed, query=True, text=True))
    for i in range(n):
        cmds.polyCone(ax=(0,0,1), n='boid'+str(i), r=1, h=3)
        cmds.setAttr('boid'+str(i)+'.translateX', random.randint(-r,r))
        cmds.setAttr('boid'+str(i)+'.translateY', random.randint(-r,r))
        cmds.setAttr('boid'+str(i)+'.translateZ', random.randint(-r,r))
        cmds.setAttr('boid'+str(i)+'.rotateX', random.randint(0,359))
        cmds.setAttr('boid'+str(i)+'.rotateY', random.randint(0,359))
        boids.append(boid('boid'+str(i),\
        om.MVector(cmds.getAttr('boid'+str(i)+'.translateX'), cmds.getAttr('boid'+str(i)+'.translateY'), cmds.getAttr('boid'+str(i)+'.translateZ')),\
        om.MVector(cmds.getAttr('boid'+str(i)+'.rotateX'), cmds.getAttr('boid'+str(i)+'.rotateY'), cmds.getAttr('boid'+str(i)+'.rotateZ'))))
        boids[i].speed = s
 
def applyCore(arg):
    global cohesionStrenght, separationStrenght, alignmentStrenght
    cohesionStrenght = float(cmds.textField(cohesionStr, query=True, text=True))
    separationStrenght = float(cmds.textField(separationStr, query=True, text=True))
    alignmentStrenght = float(cmds.textField(alignmentStr, query=True, text=True))

def applyChase(arg):
    global chaseStrenght, applyChasing
    chaseStrenght = float(cmds.textField(chaseStr, query=True, text=True))
    if(chaseStrenght != 0):
        applyChasing = True
    else:
        applyChasing = False
        
def loadChase(arg):
    global chaseTargetName, applyChasing
    #record name of current selection
    chaseTargetName = cmds.ls(sl=True)[0]
    applyChasing = True
    
def applyFlee(arg):
    global fleeStrenght, fleeRange, applyFleeing, predator
    fleeStrenght = float(cmds.textField(fleeStr, query=True, text=True))
    fleeRange = float(cmds.textField(fleeR, query=True, text=True))
    predator.speed = float(cmds.textField(predS, query=True, text=True))
    if(fleeStrenght != 0):
        applyFleeing = True
    else:
        applyFleeing = False
        
def loadFlee(arg):
    global predatorName, applyFleeing, predator
    #record name of current selection
    predatorName = cmds.ls(sl=True)[0]
    applyFleeing = True
    predator = boid(predatorName, getPositionOf(predatorName),\
    om.MVector(cmds.getAttr(predatorName+'.rotateX'), cmds.getAttr(predatorName+'.rotateY'), cmds.getAttr(predatorName+'.rotateZ')))
    predator.speed = float(cmds.textField(predS, query=True, text=True))
    
def togglePredatorState(arg):
    global dynamicPredator
    dynamicPredator = not dynamicPredator
    
def applyCurrent(arg):
    global externalCurrentHdn
    x = float(cmds.textField(currentX, query=True, text=True))
    y = float(cmds.textField(currentY, query=True, text=True))
    z = float(cmds.textField(currentZ, query=True, text=True))
    externalCurrentHdn = om.MVector(x,y,z)
    
def applyContainment(arg):
    global containmentStrenght, applyContainment
    containmentStrenght = float(cmds.textField(containmentStr, query=True, text=True))
    if(containmentStrenght != 0):
        applyContainment = True
    else:
        applyContainment = False
        
def loadContainment(arg):
    global containmentArea
    name = cmds.ls(sl=True)[0]
    width = cmds.polyCube( name, q=True, w=True )
    height = cmds.polyCube( name, q=True, h=True )
    depth = cmds.polyCube( name, q=True, d=True )
    pos = getPositionOf(name)
    # save in two vectors min and max
    # tweak values to account for boids not being points
    tweak = 5
    min = om.MVector(pos.x - width/2 + tweak, pos.y - height/2 + tweak, pos.z - depth/2 + tweak)
    max = om.MVector(pos.x + width/2 - tweak, pos.y + height/2 - tweak, pos.z + depth/2 - tweak)
    containmentArea = [min, max]
    applyContainment = True
    
def applyAvoidanceStr(arg):
    global obstacleAvoidanceStrenght, applyAvoidance
    obstacleAvoidanceStrenght = float(cmds.textField(avoidanceStr, query=True, text=True))
    if(obstacleAvoidanceStrenght != 0):
        applyAvoidance = True
    else:
        applyAvoidance = False
        
def addObstacle(arg):
    global obstacles, applyAvoidance
    # add selected obj to list
    selName = cmds.ls(sl=True)[0]
    obstacles.append(selName)
    applyAvoidance = True

# create window
windowWidth = 500
toolWindow = cmds.window(t = 'Swarm Sim', w = windowWidth);
tabs = cmds.tabLayout(innerMarginWidth=5, innerMarginHeight=5)

# SCENE CONTROL FORM
sceneLayout = cmds.formLayout()
# spawn data and button
sceneCol = cmds.columnLayout(columnAttach=('left', windowWidth/3))
cmds.rowLayout(p = sceneCol, nc = 2)
cmds.text(l='Spawn area radius: ', al='left')
radius = cmds.textField(tx = '15', w = windowWidth/10)
cmds.rowLayout(p = sceneCol, nc = 2)
cmds.text(l='Number of boids: ', al='left')
number = cmds.textField(tx = '30', w = windowWidth/10)
cmds.rowLayout(p = sceneCol, nc = 2)
cmds.text(l='Boid speed: ', al='left')
speed = cmds.textField(tx = '20', w = windowWidth/10)
resetB = cmds.button(p = sceneLayout, l='Reset and respawn boids', w = windowWidth/3, c = reset)
cmds.formLayout(sceneLayout, edit = True, attachForm = [[resetB, 'bottom', 15], [resetB, 'left', windowWidth/3]])

# ALGORITHMS FORM
cmds.setParent(tabs)
algorithmsLayout = cmds.columnLayout()
# core
cmds.setParent(algorithmsLayout)
coreFrame = cmds.frameLayout('Core', cll = True, cl = True, w = windowWidth);
cmds.rowLayout(p = coreFrame, nc = 6)
cmds.text(l='Cohesion Strenght: ', al='left')
cohesionStr = cmds.textField(tx = '0.2', w = windowWidth/10)
cmds.text(l=' Separation Strenght: ', al='left')
separationStr = cmds.textField(tx = '1', w = windowWidth/10)
cmds.text(l=' Alignment Strenght: ', al='left')
alignmentStr = cmds.textField(tx = '0.3', w = windowWidth/10)
applyCoreB = cmds.button(p = coreFrame, l='Apply', c = applyCore)
# chasing
cmds.setParent(algorithmsLayout)
chaseFrame = cmds.frameLayout('Chasing', cll = True, cl = True, w = windowWidth);
cmds.rowLayout(p = chaseFrame, nc = 3)
cmds.text(l='Chase Strenght: ', al='left')
chaseStr = cmds.textField(tx = '0', w = windowWidth/10)
applyChaseB = cmds.button(l='Apply', c = applyChase)
loadChaseB = cmds.button(p = chaseFrame, l='Load selected object as target', c = loadChase)
# fleeing
cmds.setParent(algorithmsLayout)
fleeFrame = cmds.frameLayout('Fleeing', cll = True, cl = True, w = windowWidth);
cmds.rowLayout(p = fleeFrame, nc = 7)
cmds.text(l='Flee Strenght: ', al='left')
fleeStr = cmds.textField(tx = '0', w = windowWidth/10)
cmds.text(l=' Flee Range: ', al='left')
fleeR = cmds.textField(tx = '35', w = windowWidth/10)
cmds.text(l=' Predator Speed: ', al='left')
predS = cmds.textField(tx = '40', w = windowWidth/10)
cmds.checkBox( l=' Dynamic Predator', onc = togglePredatorState, ofc = togglePredatorState)
applyFleeB = cmds.button(p = fleeFrame, l='Apply', c = applyFlee)
loadFleeB = cmds.button(p = fleeFrame, l='Load selected object as target', c = loadFlee)
# external current
cmds.setParent(algorithmsLayout)
excurrentFrame = cmds.frameLayout('External current', cll = True, cl = True, w = windowWidth);
cmds.rowLayout(p = excurrentFrame, nc = 7)
cmds.text(l='External current heading     ', al='left')
cmds.text(l=' x: ')
currentX = cmds.textField(tx = '0.0', w = windowWidth/10)
cmds.text(l=' y: ')
currentY = cmds.textField(tx = '0.0', w = windowWidth/10)
cmds.text(l=' z: ')
currentZ = cmds.textField(tx = '0.0', w = windowWidth/10)
applyCurrentB = cmds.button(p = excurrentFrame, l='Apply', c = applyCurrent)
# containment
cmds.setParent(algorithmsLayout)
containmentFrame = cmds.frameLayout('Containment', cll = True, cl = True, w = windowWidth);
cmds.rowLayout(p = containmentFrame, nc = 3)
cmds.text(l='Containment Strenght: ', al='left')
containmentStr = cmds.textField(tx = '0', w = windowWidth/10)
applyContainmentB = cmds.button(l='Apply', c = applyContainment)
loadContainmenteB = cmds.button(p = containmentFrame, l='Load selected polyCube as area', c = loadContainment)
# obstacle avoidance
cmds.setParent(algorithmsLayout)
avoidanceFrame = cmds.frameLayout('Obstacle avoidance', cll = True, cl = True, w = windowWidth);
cmds.rowLayout(p = avoidanceFrame, nc = 3)
cmds.text(l='Avoidance Strenght: ', al='left')
avoidanceStr = cmds.textField(tx = '0', w = windowWidth/10)
applyAvoidanceB = cmds.button(l='Apply', c = applyAvoidanceStr)
loadAvoidanceB = cmds.button(p = avoidanceFrame, l='Add selected polySphere as obstacle', c = addObstacle)
# add tabs and show
cmds.tabLayout(tabs, edit=True, tabLabel=((sceneLayout, 'Scene control'), (algorithmsLayout, 'Algorithms')))
cmds.showWindow()
